## Introduction ##
All tasks descriptions will be in this README.md file of the corresponding branch. The workflow should be:

Create a fork of this repository at bitbucket + give the bitbucket user Christian_Rommel access to your repo. Your fork should have the same name as this repo, challenges. 

1. We recommend working only on one task at a time. If finished with one task, ping me atk skype to get feedback. 

2. Each task has time estimation. If you are experienced in some area, some task might go a lot faster, if unexperienced, maybe a lot longer.

3. Push you final solution / answer to the corresponding feature branch. Written answers can be inserted inside the README.md directly behind the questions.

4. It is about quality, so your try to provide the final solution with your first try.


Backend developers: Only git challenge.
Frontend developers: We recommend starting with angular, then git, then node, then css task. 

----------
## task 1 - git ##
* checkout branch task/1_git
* value 2h

----------
## task 2 - css ##
* checkout branch task/2_css
* value 4h

----------
## task 3 - node ##
* checkout branch task/3_node
* value 10h


----------
## task 4 - angular ##
* checkout branch task/4_angular
* value 8h


## task 5 - BASH ##
* checkout branch task/5_bash
* value 4h

## task 6 - SQL ##
* checkout branch task/6_sql
* value 20h